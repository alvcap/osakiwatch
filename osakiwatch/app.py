import sys
import os
import pkg_resources
import time

from PyQt5 import QtWidgets, QtCore, uic
import pandas as pd
from IPython import embed

from .allocator import Allocator


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        current_dir = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(os.path.join(current_dir, "qt/main_window.ui"), self)

        self._edit_r1_data_widget = EditR1Data()
        self._edit_weights_widget = EditWeights()
        self._edit_hospitals_widget = EditHospitals()
        self._allocator = Allocator()

        self._edit_r1_data_button = self.findChild(QtWidgets.QPushButton, "editR1DataButton")
        self._edit_r1_data_button.clicked.connect(self.edit_r1_data) 

        self._edit_weights_button = self.findChild(QtWidgets.QPushButton, "editWeightsButton")
        self._edit_weights_button.clicked.connect(self.edit_weights) 

        self._edit_hospitals_button = self.findChild(QtWidgets.QPushButton, "editHospitalsButton")
        self._edit_hospitals_button.clicked.connect(self.edit_hospitals) 

        self._allocate_watches_button = self.findChild(QtWidgets.QPushButton, "allocateWatchesButton")
        self._allocate_watches_button.clicked.connect(self.allocate_watches)

        self._calendar_widget = self.findChild(QtWidgets.QCalendarWidget, "calendarWidget")

        version = pkg_resources.get_distribution("osakiwatch").version
        self.setWindowTitle("OSAKI watch v." + version)
        self.show()

    def edit_r1_data(self):
        self._edit_r1_data_widget.populate_table()
        self._edit_r1_data_widget.show()

    def edit_weights(self):
        self._edit_weights_widget.populate_table()
        self._edit_weights_widget.show()

    def edit_hospitals(self):
        self._edit_hospitals_widget.populate_table()
        self._edit_hospitals_widget.show()

    def allocate_watches(self):
        self._allocator.month = self._calendar_widget.monthShown()
        self._allocator.year = self._calendar_widget.yearShown()

        self._allocate_watches_button.setText("Calculando, espere...")
        self._allocate_watches_button.repaint()

        self._allocator.allocate_month()

        self._allocate_watches_button.setText("Hecho")
        self._allocate_watches_button.repaint()
        time.sleep(1)
        self._allocate_watches_button.setText("Asignar Guardias")
        self._allocate_watches_button.repaint()

class EditR1Data(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(EditR1Data, self).__init__(parent)

        current_dir = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(os.path.join(current_dir, "qt/edit_r1_data.ui"), self)

        self._allocator = Allocator()
        self._r1_data_table_widget = self.findChild(QtWidgets.QTableWidget, "r1DataTableWidget")

        self._r1_data_table_widget.setHorizontalHeaderLabels(["Nombre", "Guardias al mes", "Excluir dias", "Puede trauma"])
        self._r1_data_table_widget.itemDoubleClicked.connect(self._r1_data_table_widget.editItem)

        self._add_row_button = self.findChild(QtWidgets.QPushButton, "addRowButton")
        self._add_row_button.clicked.connect(self._add_row)

        self._remove_row_button = self.findChild(QtWidgets.QPushButton, "removeRowButton")
        self._remove_row_button.clicked.connect(self._remove_rows)

        quit = QtWidgets.QAction("Quit", self)
        quit.triggered.connect(self.closeEvent)

    def populate_table(self):
        self._allocator.get_data()
        for i, field in self._allocator.r1_data.iterrows():
            num_rows = self._r1_data_table_widget.rowCount()
            self._r1_data_table_widget.insertRow(num_rows)

            self._r1_data_table_widget.setItem(num_rows, 0, QtWidgets.QTableWidgetItem(field["name"]))
            self._r1_data_table_widget.setItem(num_rows, 1, QtWidgets.QTableWidgetItem(str(field["watches_pr_month"])))

            #if type(field["exclude_week"]) == list:
            #    exclude_week = ", ".join(list(map(str, field["exclude_week"])))
            #else:
            #    exclude_week = str(field["exclude_week"])
            #self._r1_data_table_widget.setItem(num_rows, 2, QtWidgets.QTableWidgetItem(exclude_week))

            if type(field["exclude_days"]) == list:
                exclude_days = ", ".join(list(map(str, field["exclude_days"])))
            else:
                exclude_days = str(field["exclude_days"])
            self._r1_data_table_widget.setItem(num_rows, 2, QtWidgets.QTableWidgetItem(exclude_days))
            self._r1_data_table_widget.setItem(num_rows, 3, QtWidgets.QTableWidgetItem(str(field["trauma"])))

        self._r1_data_table_widget.resizeColumnsToContents()

    def _add_row(self):
        num_rows = self._r1_data_table_widget.rowCount()
        self._r1_data_table_widget.insertRow(num_rows)

    def _remove_rows(self):
        for item in self._r1_data_table_widget.selectedItems():
            self._r1_data_table_widget.removeRow(item.row())

    def save_data(self):
        num_rows = self._r1_data_table_widget.rowCount()
        rows = []
        for row in range(0, num_rows):
            row_data = {}
            row_data["name"] = self._r1_data_table_widget.item(row, 0).text()
            row_data["watches_pr_month"] = int(self._r1_data_table_widget.item(row, 1).text())

            #exclude_week = self._r1_data_table_widget.item(row, 2).text()
            #if exclude_week != "None":
            #    exclude_week = [int(week_str) for week_str in exclude_week.replace(" ", "").split(",")]
            #else:
            #    exclude_week = [exclude_week]
            #row_data["exclude_week"] = exclude_week

            exclude_days = self._r1_data_table_widget.item(row, 2).text()
            if exclude_days != "None":
                exclude_days = [int(day_str) for day_str in exclude_days.replace(" ", "").split(",")]
            else:
                exclude_days = [exclude_days]
            row_data["exclude_days"] = exclude_days
            row_data["trauma"] = (self._r1_data_table_widget.item(row, 3).text() in ["True", "true"])
            rows.append(row_data)

        r1_data = pd.DataFrame(rows)
        self._allocator.save_r1_data(r1_data)

    def closeEvent(self, event):
        close = QtWidgets.QMessageBox.question(self,
                                         "Closing",
                                         "Do you want to save current data?",
                                         QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        if close == QtWidgets.QMessageBox.Yes:
            self.save_data()

        for row in reversed(range(self._r1_data_table_widget.rowCount())):
            self._r1_data_table_widget.removeRow(row)

        event.accept()

class EditWeights(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(EditWeights, self).__init__(parent)

        current_dir = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(os.path.join(current_dir, "qt/edit_weights.ui"), self)

        self._allocator = Allocator()
        self._weights_table_widget = self.findChild(QtWidgets.QTableWidget, "weightsTableWidget")

        self._weights_table_widget.setHorizontalHeaderLabels(["Nombre", "Valor"])

        self._add_row_button = self.findChild(QtWidgets.QPushButton, "addRowButton")
        self._add_row_button.clicked.connect(self._add_row)

        self._remove_row_button = self.findChild(QtWidgets.QPushButton, "removeRowButton")
        self._remove_row_button.clicked.connect(self._remove_rows)

        quit = QtWidgets.QAction("Quit", self)
        quit.triggered.connect(self.closeEvent)

    def populate_table(self):
        self._allocator.get_data()
        for i, field in self._allocator.weights.iterrows():
            num_rows = self._weights_table_widget.rowCount()
            self._weights_table_widget.insertRow(num_rows)
            self._weights_table_widget.setItem(num_rows, 0, QtWidgets.QTableWidgetItem(field["name"]))
            self._weights_table_widget.setItem(num_rows, 1, QtWidgets.QTableWidgetItem(str(field["value"])))

        self._weights_table_widget.resizeColumnsToContents()

    def _add_row(self):
        num_rows = self._weights_table_widget.rowCount()
        self._weights_table_widget.insertRow(num_rows)

    def _remove_rows(self):
        for item in self._weights_table_widget.selectedItems():
            self._weights_table_widget.removeRow(item.row())

    def save_data(self):
        num_rows = self._weights_table_widget.rowCount()
        rows = []
        for row in range(0, num_rows):
            row_data = {}
            row_data["name"] = self._weights_table_widget.item(row, 0).text()
            row_data["value"] = float(self._weights_table_widget.item(row, 1).text())
            rows.append(row_data)

        weights = pd.DataFrame(rows)
        self._allocator.save_weights(weights)

    def closeEvent(self, event):
        close = QtWidgets.QMessageBox.question(self,
                                         "Closing",
                                         "Do you want to save current data?",
                                         QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        if close == QtWidgets.QMessageBox.Yes:
            self.save_data()

        for row in reversed(range(self._weights_table_widget.rowCount())):
            self._weights_table_widget.removeRow(row)

        event.accept()

class EditHospitals(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(EditHospitals, self).__init__(parent)

        current_dir = os.path.dirname(os.path.abspath(__file__))
        uic.loadUi(os.path.join(current_dir, "qt/edit_hospitals.ui"), self)

        self._allocator = Allocator()
        self._hospitals_table_widget = self.findChild(QtWidgets.QTableWidget, "hospitalsTableWidget")

        self._hospitals_table_widget.setHorizontalHeaderLabels(["Nombre", "Secciones", "Max. guardias"])

        self._add_row_button = self.findChild(QtWidgets.QPushButton, "addRowButton")
        self._add_row_button.clicked.connect(self._add_row)

        self._remove_row_button = self.findChild(QtWidgets.QPushButton, "removeRowButton")
        self._remove_row_button.clicked.connect(self._remove_rows)

        quit = QtWidgets.QAction("Quit", self)
        quit.triggered.connect(self.closeEvent)

    def populate_table(self):
        self._allocator.get_data()
        for i, field in self._allocator._hospitals_data.iterrows():
            num_rows = self._hospitals_table_widget.rowCount()
            self._hospitals_table_widget.insertRow(num_rows)
            self._hospitals_table_widget.setItem(num_rows, 0, QtWidgets.QTableWidgetItem(field["name"]))

            if type(field["sections"]) == list:
                sections = ", ".join(field["sections"])
            else:
                sections = field["sections"]
            self._hospitals_table_widget.setItem(num_rows, 1, QtWidgets.QTableWidgetItem(sections))
            self._hospitals_table_widget.setItem(num_rows, 2, QtWidgets.QTableWidgetItem(str(field["max_watches"])))

        self._hospitals_table_widget.resizeColumnsToContents()

    def _add_row(self):
        num_rows = self._hospitals_table_widget.rowCount()
        self._hospitals_table_widget.insertRow(num_rows)

    def _remove_rows(self):
        for item in self._hospitals_table_widget.selectedItems():
            self._hospitals_table_widget.removeRow(item.row())

    def save_data(self):
        num_rows = self._hospitals_table_widget.rowCount()
        rows = []
        for row in range(0, num_rows):
            row_data = {}
            row_data["name"] = self._hospitals_table_widget.item(row, 0).text()
            row_data["sections"] = self._hospitals_table_widget.item(row, 1).text().replace(" ", "").split(",")
            max_watches = self._hospitals_table_widget.item(row, 2).text()
            row_data["max_watches"] = (float(max_watches) if self._hospitals_table_widget.item(row, 2).text() not in ["nan", "None"] else "None")
            rows.append(row_data)

        hospitals = pd.DataFrame(rows)
        self._allocator.save_hospitals(hospitals)

    def closeEvent(self, event):
        close = QtWidgets.QMessageBox.question(self,
                                         "Closing",
                                         "Do you want to save current data?",
                                         QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        if close == QtWidgets.QMessageBox.Yes:
            self.save_data()

        for row in reversed(range(self._hospitals_table_widget.rowCount())):
            self._hospitals_table_widget.removeRow(row)

        event.accept()

def run():
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    app.exec_()

if __name__ == "__main__":
    run()