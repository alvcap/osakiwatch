import os
import sys
import json
import math
import calendar
from datetime import date
from random import shuffle
from pkg_resources import resource_string

import pandas as pd


from IPython import embed

MAX_WATCHES_PR_WEEK = 2
MAX_WEEKEND_WATCHES = 2
MIN_GAP_BETWEEN_WATCHES = 5     # Allow x days gap between watches

class Allocator(object):
    def __init__(self):
        self._r1_data = None
        self._weights = None
        self._allocation = pd.DataFrame(columns=["date", "name", "hospital", "section"])

        self._day = None
        self._month = None
        self._year = None

    def get_data(self):
        json_r1_data_path = get_save_path("r1.json")
        json_weights_path = get_save_path("weights.json")
        json_hospitals_path = get_save_path("hospitals.json")

        exec_path = os.path.dirname(os.path.abspath(__file__))
        
        if not os.path.isfile(json_r1_data_path):
            json_r1_data_path = os.path.join(exec_path , "data", "r1.json")
        if not os.path.isfile(json_weights_path):
            json_weights_path = os.path.join(exec_path , "data", "weights.json")
        if not os.path.isfile(json_hospitals_path):
            json_hospitals_path = os.path.join(exec_path , "data", "hospitals.json")

        with open(json_r1_data_path, "r") as f:
            self._r1_data = pd.io.json.json_normalize(json.loads(f.read())["r1-data"])
        with open(json_weights_path, "r") as f:
            self._weights = pd.io.json.json_normalize(json.loads(f.read())["weights"])
        with open(json_hospitals_path, "r") as f:
            self._hospitals_data = pd.io.json.json_normalize(json.loads(f.read())["hospitals"])

    def save_r1_data(self, r1_data):
        save_path = get_save_path("r1.json")
        data = {}

        # Save lists as lists of ints
        r1_data_dict = r1_data.to_dict(orient="records")
        for i,  rec in enumerate(r1_data_dict):
            #exclude_week = rec["exclude_week"]
            #if len(exclude_week) == 1:
            #    exclude_week = (int(exclude_week[0]) if exclude_week[0] != "None" else "None")
            #else:
            #    exclude_week = [int(val) for val in exclude_week]

            #r1_data_dict[i]["exclude_week"] = exclude_week

            exclude_days = rec["exclude_days"]
            if len(exclude_days) == 1:
                exclude_days = (int(exclude_days[0]) if exclude_days[0] != "None" else "None")
            else:
                exclude_days = [int(val) for val in exclude_days]

            r1_data_dict[i]["exclude_days"] = exclude_days

        data["r1-data"] = r1_data_dict
        with open(save_path, "w", encoding="utf-8") as f:
            json_str = json.dumps(data, indent=4, ensure_ascii=False)
            f.write(json_str)

    def save_weights(self, weights):
        save_path = get_save_path("weights.json")
        data = {}
        data["weights"] = weights.to_dict(orient="records")
        with open(save_path, "w", encoding="utf-8") as f:
            json_str = json.dumps(data, indent=4, ensure_ascii=False)
            f.write(json_str)

    def save_hospitals(self, hospitals):
        save_path = get_save_path("hospitals.json")
        data = {}
        data["hospitals"] = hospitals.to_dict(orient="records")
        with open(save_path, "w", encoding="utf-8") as f:
            json_str = json.dumps(data, indent=4, ensure_ascii=False)
            f.write(json_str)

    def allocate_month(self):
        self._allocation = self._allocation.iloc[0:0]
        # Initialize days and weights pr R1
        self._day = 1

        self.get_data()
        self._r1_data["burden"] = 0

        first_day = date(self._year, self._month, self._day)
        last_day = add_months(first_day, 1)

        delta = last_day-first_day

        while self._day <= delta.days:
            self._allocate_day(self._day)
            self._day += 1

        save_path = os.path.join(os.environ["HOMEPATH"], "Desktop", 
                    "guardias-{0}-{1}.csv".format(self.month, self.year))

        self._allocation.to_csv(save_path)

        summary = self._prepare_result_summary()

        save_path = os.path.join(os.environ["HOMEPATH"], "Desktop", 
                    "summary-{0}-{1}.csv".format(self.month, self.year))

        summary.to_csv(save_path)

    def _allocate_day(self, day_nr):
        today = date(self.year, self.month, day_nr)

        for i, hospital in self._hospitals_data.iterrows():
            for section in hospital["sections"]:
                # Get a candidate using weekend filter
                candidates = self._get_possible_candidates(day_nr, self._r1_data, hospital, section, True)
                
                if candidates.empty:
                    # If it is friday, try to assign it again without using weekend filter
                    if today.weekday == 4:
                        # Get a candidate using weekend filter
                        candidates = self._get_possible_candidates(day_nr, self._r1_data, hospital, section, False)
                        if candidates.empty:
                            candidate_name = "R2"
                    else:
                        candidate_name = "R2"
                else:
                    # Of all candidates, get the ones with the least burden
                    least_burdened_candidate = self._get_least_burdened_candidate(candidates, section)

                    candidate_name = least_burdened_candidate["name"]
                    # Assign day to candidates
                    self._save_burden_after_allocation(least_burdened_candidate, today)

                self._allocation = self._allocation.append({ "date": today,
                                          "name": candidate_name ,  
                                          "hospital": hospital["name"],
                                          "section": section }, ignore_index=True)


    def _get_possible_candidates(self, day, init_candidates, hospital, section, filter_weekends):
        """ 
        Return DF of candidates that can work this day
    
        Some people will not be able to work some days, filter them out here, burden is not taken into account here

        Reasons for not being a candidate:
          - Is already allocated today
          - Is specifically excluded from working today
          - Today is too close to previous watch assigned to current person
          - Has done the max amount of watches required by the hospital
          - The candidate is not supposed to work on trauma
          - Has already made two watches this week. This one would be the third, and that is not desired
          - Has made a watch on a friday, and today is saturday
          - Has made a watch on a saturday and today is sunday
          - Has made his max number of watches
        """

        possible_candidates = pd.DataFrame()
        today = date(self.year, self.month, day)
        current_week = int(today.strftime("%W"))
        day_of_the_week = today.weekday()

        for i, candidate in init_candidates.iterrows():
            keep_candidate = True
            candidate_history = pd.DataFrame()

            if not self._allocation.empty:
                candidate_history = self._allocation.loc[self._allocation["name"]==candidate["name"]]
                watches_in_hospital = candidate_history.loc[candidate_history["hospital"]==hospital["name"]]
                watches_on_weekend = calculate_watches_on_weekend(candidate_history)

                exclude_days = candidate["exclude_days"]
                if exclude_days:
                    if type(exclude_days) == list:
                        if today.day in exclude_days:
                            keep_candidate *= False
                    else:
                        if today.day == exclude_days:
                            keep_candidate *= False

                # Has done max amount of watches required by the hospital
                try:
                    if not math.isnan(hospital["max_watches"]):
                        if watches_in_hospital.shape[0] == hospital["max_watches"]:
                            keep_candidate *= False
                except:
                    if hospital["max_watches"] != "None":
                        if watches_in_hospital.shape[0] == hospital["max_watches"]:
                            keep_candidate *= False

            # Candidate is not supposed to work on trauma
            if (candidate["trauma"]==False) and (section=="trauma"):
                keep_candidate *= False

            
            if candidate["watches_pr_month"] == 0:
                keep_candidate *= False

            if not candidate_history.empty:
                # This week is excluded for this person
                #exclude_week = candidate["exclude_week"]
                #if exclude_week:
                #    if type(exclude_week) == list:
                #        # Datetime starts counting weeks from 0
                #        if current_week+1 in exclude_week:
                #            keep_candidate *= False
                #    else:
                #        if current_week+1 == exclude_week:
                #            keep_candidate *= False

                # Is already allocated today
                if today in candidate_history["date"].values:
                    keep_candidate *= False

                last_watch_date =  candidate_history["date"].sort_values(ascending=False).iloc(0)[0]
                if ((today-last_watch_date).days <= MIN_GAP_BETWEEN_WATCHES):
                    keep_candidate *= False

                if filter_weekends:
                    # Count number of weekends worked by candidate
                    weekends_worked = 0
                    for watch_date in candidate_history["date"].values:
                        wd = watch_date.weekday()
                        # In this context, we count friday as weekend
                        if wd in [4, 5, 6]:
                            weekends_worked += 1

                    # Discard candidate if max number of weekends has already been assigned
                    if weekends_worked > MAX_WEEKEND_WATCHES:
                        keep_candidate *= False

                # Candidate has already worked the max. allowed watches per month
                total_watches = candidate_history.shape[0]
                if total_watches == candidate["watches_pr_month"]:
                    keep_candidate *= False
                
                watches_this_week = 0
                for watch_date in candidate_history["date"]:
                    if watch_date.strftime("%W") == current_week:
                        watches_this_week += 1

                    # If saturday or sunday and already made watch on friday or saturday
                    if day_of_the_week in [5, 6]:
                        if (watch_date.weekday()+1) == day_of_the_week:
                            keep_candidate *= False

                    else:
                        # If the person has made one third of his max allowed watches and 
                        # we are not in weekend yet, reserve him for next weekend
                        one_third_of_candidates_max = candidate["watches_pr_month"]/3
                        if total_watches >= one_third_of_candidates_max and watches_on_weekend == 0:
                            keep_candidate *= False

                # If assigning this watch would result in exceeding max. number of watches pr. week
                if watches_this_week >= MAX_WATCHES_PR_WEEK:
                    keep_candidate *= False

            # After all filters are executed, evaluate if candidate can be assigned the watch or not
            if keep_candidate:
                possible_candidates = possible_candidates.append(candidate)

        return possible_candidates

    def _save_burden_after_allocation(self, candidate, date_obj):
        burden = self._r1_data.loc[self._r1_data.name==candidate["name"], "burden"].iloc[0]
        
        weekday = date_obj.weekday()

        # Weekend
        if weekday in [5, 6]:
            weight = self._weights.loc[self._weights["name"]=="weekend"]["value"].iloc[0]

        # Monday
        elif weekday == 0:
            weight = self._weights.loc[self._weights["name"]=="monday"]["value"].iloc[0]

        # Thursday
        elif weekday == 3:
            weight = self._weights.loc[self._weights["name"]=="thursday"]["value"].iloc[0]

        # Friday
        elif weekday == 4:
            weight = self._weights.loc[self._weights["name"]=="friday"]["value"].iloc[0]

        # Normal day
        else:
            weight = self._weights.loc[self._weights["name"]=="normal_day"]["value"].iloc[0]

        burden += weight
        self._r1_data.at[self._r1_data.name==candidate["name"], "burden"] = burden

    # Of all R1s, returns the ones that have the smallest burden
    def _get_least_burdened_candidate(self, init_candidates, section):
        # Get the three least burdened
        init_candidates.sort_values("burden")

        # Candidates with smallest burden
        least_burdened = init_candidates.loc[init_candidates["burden"] == init_candidates["burden"].min()]
        if least_burdened.shape[0] <= 3:
            # Get the three smallest ones
            least_burdened_candidates = init_candidates.copy()[0:3]
        else:
            # Shuffle and get three
            least_burdened = least_burdened.sample(frac=1).reset_index(drop=True)
            least_burdened = least_burdened.sample(frac=1).reset_index(drop=True)
            least_burdened_candidates = least_burdened.copy()[0:3]

        # Of the least burdened candidates, get the one that has worked least in this section
        if not self._allocation.empty:
            for i, candidate in least_burdened_candidates.iterrows():
                candidate_history = self._allocation.loc[self._allocation["name"]==candidate["name"]]
                watches_in_section = candidate_history.loc[candidate_history["section"]==section]

                num = watches_in_section.shape[0]
                least_burdened_candidates.at[i,"watches_in_section"] = num

            least_burdened_candidates.sort_values("watches_in_section")        

        return least_burdened_candidates.iloc[0]

    def _prepare_result_summary(self):
        summary = pd.DataFrame()
        summary_dict = {}
        summary_dict["name"] = []
        summary_dict["total_watches"] = []
        summary_dict["weekends"] = []
        summary_dict["mondays"] = []
        summary_dict["thursdays"] = []
        summary_dict["fridays"] = []
        hospital_names = self._allocation["hospital"].unique()
        for hospital_name in hospital_names:
            summary_dict[hospital_name] = []

        for name in self._allocation["name"].unique():
            person_history = self._allocation.loc[self._allocation["name"]==name]
            summary_dict["name"].append(name)
            summary_dict["total_watches"].append(person_history.shape[0])

            for hospital_name in hospital_names:
                summary_dict[hospital_name].append(person_history.loc[person_history["hospital"]==hospital_name]["hospital"].count())

            weekdays = [watch_date.weekday() for watch_date in person_history["date"]]

            summary_dict["weekends"].append(weekdays.count(5) + weekdays.count(6))
            summary_dict["mondays"].append(weekdays.count(0))
            summary_dict["thursdays"].append(weekdays.count(3))
            summary_dict["fridays"].append(weekdays.count(4))

        summary = summary.from_dict(summary_dict)

        return summary

    @property
    def r1_data(self):
        return self._r1_data

    @property
    def weights(self):
        return self._weights

    @property
    def month(self):
        return self._month

    @month.setter
    def month(self, month):
        self._month = month

    @property
    def year(self):
        return self._year

    @year.setter
    def year(self, year):
        self._year = year



def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year,month)[1])

    return date(year, month, day)

def calculate_watches_on_weekend(candidate_history):
    watches_on_weekend = 0
    for watch_date in candidate_history["date"]:
        if watch_date.weekday() in [5,6]:
            watches_on_weekend += 1
    
    return watches_on_weekend

def get_save_path(filename):
    save_dir = os.path.join(os.environ["HOMEPATH"], ".osakiwatch")
    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)
    save_path = os.path.join(save_dir, filename)

    return save_path



if __name__ == "__main__":
    alloc = Allocator()
    alloc.month = 4
    alloc.year = 2020
    alloc.allocate_month()