# OSAKI-WATCH

Distribuidor automatico de guardias para residentes

Requisitos: Descargar e instalar python. https://www.python.org/downloads/windows/

Una vez instalado python, descargar osakiwatch. Download -> zip. Descomprimir.

Para instalar osakiwatch, doble click en:
```sh
install.bat
```

Para usar, doble click en:
```sh
start-osakiwatch.bat
```