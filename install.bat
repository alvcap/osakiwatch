rmdir "build"
rmdir "dist"
rmdir "*egg-info"
python setup.py install
robocopy "osakiwatch/data" "build/lib/osakiwatch/data/" /E
robocopy "osakiwatch/qt" "build/lib/osakiwatch/qt" /E
python setup.py install
pip uninstall PyQt5
pip uninstall PyQt5-sip
pip install PyQt5