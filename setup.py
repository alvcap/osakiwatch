from setuptools import setup

setup(name="osakiwatch",
      version="0.0.3",
      description="Watch assigning program for doctors",
      author="alvcap",
      author_email="kape013@gmail.com",
      url="https://gitlab.com/alvcap/osakiwatch",
      packages=["osakiwatch"],
      provides=["osakiwatch"],
      package_data={"osakiwatch": ["data/*", "qt/*"]},
      license="GNU General Public License v3",

      install_requires=["PyQt5", "pyqt5_tools"],
      entry_points={"console_scripts":
                    ["osakiwatch = osakiwatch.app:run"]
                    },
      classifiers=[
            "Programming Language :: Python",
            "Programming Language :: Python :: 3",
            "Development Status :: 4 - Beta",
            "Intended Audience :: Developers",
            "Operating System :: OS Independent",
            "Intended Audience :: Healthcare Industry"
      ]
      )